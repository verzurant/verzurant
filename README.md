# Verzurant

A personal Hugo theme, not yet intended for consumption by others.

You can view this theme's [CHANGELOG here](/_meta/CHANGELOG.md).

At the moment, this theme's [LICENSE file](/LICENSE) is still in this root directory, rather than the `_meta` subdirectory.

You can view this theme's [STYLEGUIDE here](/_meta/STYLEGUIDE.md).

This theme seeks to avoid, as much as possible, re-inventing functionality and/or templating already embedded by default into the Hugo core. However, some Hugo core default configurations may be included in the `exampleSite`, both to document their existence, and to easily enable changing them.

## About the `exampleSite` / Installing This Theme

You can view the downstream demo site's [repository here](https://gitlab.com/verzurant/verzurant.gitlab.io/), and its [live instance here](https://verzurant.gitlab.io/). *That* repository is simply a mirror of the `exampleSite` directory contained within the current *theme* repository you are viewing now.

Incidentally, the most straightforward way of *using* this theme is to simply *clone* the above demo site mirror repository. Then just delete the downloaded `.git` folder, change the `content`, `config`, and/or `static` directory files to your liking, and if desired, *override* other files in this upstream theme repository by creating files with the same names in, e.g., `layouts` and/or `assets` folders in your own site project repo (no `data` folder is yet used in either the theme itself, or the `exampleSite`).

FYI going forward the Hugo team seems to favor using [Hugo Modules](https://discourse.gohugo.io/t/hugo-modules-for-dummies/20758/3) for adding themes to your site. The `exampleSite` is conceived with this in mind, and using a `git submodule` to add this theme to your site is discouraged. To learn more about Hugo Modules, [start here](https://gohugo.io/hugo-modules/).

Note that the GitLab CI configuration file in the `exampleSite` uses a different CI image, as the stock image for [GitLab Pages](https://gohugo.io/hosting-and-deployment/hosting-on-gitlab/) does not yet [support](https://gitlab.com/pages/hugo/-/issues/53) Hugo Modules (in esssence, the [stock image](https://discourse.gohugo.io/t/gitlab-ci-hugo-module-error/28956) does not contain a `go` executable). The `.gitlab-ci.yml` file in `exampleSite` may change in the future.
