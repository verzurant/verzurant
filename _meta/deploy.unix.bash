#!/bin/bash

#-------------------------------------------------------------------------------
# this lo-fi script is for manually syncing the `exampleSite` directory to
# the separate theme demo repository after commits which affect the former
#-------------------------------------------------------------------------------

# variables are defined in .envrc for now
rsync --recursive $V_EXMP $V_DEMO

cd $V_DEMO
git add .

# get current commit hash in theme repo?
git commit -am "[Chore] manual sync from theme_repo/exampleSite"

git push lab main
