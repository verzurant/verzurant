# Markdown Links On GitLab.com

Currently, attempting to arbitrarily create links that open in a new tab still seems to be unsupported on public-facing GitLab.com in, e.g., `README.md` files. This is in spite of a special (outdated?) syntax [mentioned](https://about.gitlab.com/handbook/markdown-guide/#inline-links) on their site:

`[Text to display](link){:target="_blank"}`

Maybe that guide *only* applies to a different scenario for *internal* `GitLab.com` pages? More GitLab-specific info is [presented here](https://docs.gitlab.com/ee/user/markdown.html#links).

Of note though, once rendered on `GitLab.com`, the basic `[text](url)` syntax *does* seem to give special treatment to *external links* (they *will* open in a new tab); to clarify, "external" in this case only means any URL not on the `GitLab.com` domain (so linking to an external *repository* that is still hosted on `GitLab.com` will *not* open in a new tab).