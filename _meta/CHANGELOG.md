# PRE-RELEASE (ongoing)

### General Notes

Initially, this is a fork of the [Tania Hugo theme](https://github.com/WingLim/hugo-tania), but some primary differences include:
- Replacing single YAML config file with a config directory containing multi-environment config files in TOML format
- Splitting the `head.html` partial into multiple sub-partials, and in some cases only including some of these based on config param choices


From the [MemE Hugo theme](https://github.com/reuixiy/hugo-theme-meme), also have added
- Google Analytics ideas (but still using the internal Hugo template)
- Will revisit the favicon and feeds ideas in MemE later