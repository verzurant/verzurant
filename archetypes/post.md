+++
#-------------------------------------------------------------------------------
# this is the TOML front matter format
# https://gist.github.com/oconnor663/9aeb4ed56394cb013a20
# https://gohugo.io/content-management/front-matter/
# https://gohugo.io/content-management/archetypes/
# https://gohugo.io/content-management/types/
# https://discourse.gohugo.io/t/using-site-variables-in-front-matter/7928
#
#-------------------------------------------------------------------------------

# in post list page using Datatables, cells with this default weight are hidden,
# and the "pinned" attribute is not set in the post single metadata template
weight = 99999

toc = false # Controls if a table of contents should be generated for first-level links automatically.
# how are second-level links and deeper handled, if at all?

author = "{{ .Site.Author.default.name }}"

# `type` must be used with `layout` if you want it to use the desired template
# type = "section"
# layout = "archives"

# series = [] # separate terms with commas, quoting unnecessary
# categories = [ "Category1", "Category1" ] # separate terms with commas, quoting unnecessary
tags = [ "verzurant" ] # separate terms with commas, quoting unnecessary

# description = "Article search engine description."
# aliases = [ "old-file-name-1", "old-file-name-2" ] # uncomment if you rename anything already crawled
title = "{{ replace .Name "-" " " | title }}" # Title of the blog post.

# for timestamps, enable git options when multiple authors will edit on their own local file systems?
# use .envrc to `export TZ="UTC"`
date = {{ .Date }} # timestamp of post creation
# publishDate = {{ .Date }} # may optionally be later than actual creation (presumably a draft til "published")
# lastmod = {{ .Date }} # set this to override Hugo's own automatically generated lastmod timestamp
# expiryDate = {{ .Date }}

draft = true # Sets whether to render this page. Draft of true will not be rendered.
#-------------------------------------------------------------------------------
+++

**Insert Lead paragraph here.**

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin egestas dolor commodo, sollicitudin lorem semper, tristique elit. Vestibulum at dignissim dolor, non sollicitudin orci. Morbi sit amet massa et tellus bibendum posuere. Phasellus sit amet enim in velit posuere dapibus. Quisque sollicitudin neque malesuada eros imperdiet, at ornare nisl tincidunt. Proin in imperdiet urna. Lorem ipsum dolor sit amet, consectetur adipiscing elit.